function! Transsymb(...) dict
    if empty(a:000)
        return deepcopy(s:transsymb)
    else
        let s:transsymb=deepcopy(get(a:000, 0))
    endif
endfunction
function! s:Transsymb(...) dict
    if empty(a:000)
        return deepcopy(s:transsymb)
    else
        let s:transsymb=deepcopy(get(a:000, 0))
    endif
endfunction
let g:transsymb={}
let b:transsymb={}
let s:transsymb={}
let g:transsymb2={}
unlet g:tr3Options
let b:tr3Options={}
let b:tr3Options.ConfigDir=expand('<sfile>:h')
call writefile(['{}'],         './test-transsymb.json',  'b')
call writefile(['{}'],         './test-transsymb2.json', 'b')
call writefile(['{"a": "α"}'], './test-alpha.json',      'b')
let addremovesources=[
            \['file', fnamemodify('./test-transsymb.json', ':p')],
            \['var', 'g:transsymb'],
            \['var', 'b:transsymb'],
            \['dict', g:transsymb2],
            \['func', function('Transsymb')],
            \['conf', 'test-transsymb2'],
        \]
let addremovetests=[
            \['add', ['abc', '*', 0],
            \ [['abc', '*'],
            \  ['ab c', 'ab c']],
            \ {'a':{'b':{'c': '*'}}}],
            \['add', ['abc', '+', 0],
            \ [['abc', '*'],
            \  ['ab c', 'ab c']],
            \ {'a':{'b':{'c': '*'}}}],
            \['add', ['abc', '+', 1],
            \ [['abc', '+'],
            \  ['ab c', 'ab c']],
            \ {'a':{'b':{'c': '+'}}}],
            \['add', ['abcd', '-', 1],
            \ [['abcd', '-'],
            \  ['abc', '+']],
            \ {'a':{'b':{'c': {'none': '+', 'd': '-'}}}}],
            \['add', ['ghi', '*', 0],
            \ [['ghi', '*'],
            \  ['GHI', '*'],
            \  ['GHi', '*']],
            \ {'a':{'b':{'c': {'none': '+', 'd': '-'}}},
            \  'g':{'h':{'i': '*'}}}],
            \['add', ['ab', '@', 0],
            \ [['ab', '@'],
            \  ['abc', '+']],
            \ {'a':{'b':{'none': '@','c': {'none': '+', 'd': '-'}}},
            \  'g':{'h':{'i': '*'}}}],
            \['add', ['ab', '%', 0],
            \ [['ab', '@'],
            \  ['abc', '+']],
            \ {'a':{'b':{'none': '@','c': {'none': '+', 'd': '-'}}},
            \  'g':{'h':{'i': '*'}}}],
            \['add', ['ab', '%', 1],
            \ [['ab', '%'],
            \  ['abc', '+']],
            \ {'a':{'b':{'none': '%','c': {'none': '+', 'd': '-'}}},
            \  'g':{'h':{'i': '*'}}}],
            \['del', ['ab', 0],
            \ [['ab', 'ab'],
            \  ['abc', '+']],
            \ {'a':{'b':{'c': {'none': '+', 'd': '-'}}},
            \  'g':{'h':{'i': '*'}}}],
            \['del', ['abcd', 0],
            \ [['ab', 'ab'],
            \  ['abc', '+'],
            \  ['abcd', '+d']],
            \ {'a':{'b':{'c': {'none': '+'}}},
            \  'g':{'h':{'i': '*'}}}],
            \['del', ['a', 0],
            \ [['ab', 'ab'],
            \  ['abc', '+'],
            \  ['abcd', '+d']],
            \ {'a':{'b':{'c': {'none': '+'}}},
            \  'g':{'h':{'i': '*'}}}],
            \['del', ['a', 1],
            \ [['ab', 'ab'],
            \  ['abc', 'abc'],
            \  ['GHI', '*'],
            \  ['ghi', '*']],
            \ {'g':{'h':{'i': '*'}}}],
            \['setoption', ['capital', 'none', 'ghi', 0],
            \ [['GHI', 'GHI'],
            \  ['ghi', '*']],
            \ {'g':{'h':{'i': {'none': '*', 'options': {'capital': 'none'}}}}}],
            \['setoption', ['capital', 'first', 'ghi', 0],
            \ [['GHI', 'GHI'],
            \  ['ghi', '*']],
            \ {'g':{'h':{'i': {'none': '*', 'options': {'capital': 'none'}}}}}],
            \['setoption', ['capital', 'first', 'ghi', 1],
            \ [['GHI', '*'],
            \  ['ghi', '*']],
            \ {'g':{'h':{'i': {'none': '*', 'options': {'capital': 'first'}}}}}],
            \['deloption', ['capital', 'ghi'],
            \ [['GHI', '*'],
            \  ['ghi', '*']],
            \ {'g':{'h':{'i': {'none': '*'}}}}],
            \['setoption', ['capital', 'none', 'gh', 0],
            \ [['GHI', 'GHI'],
            \  ['ghi', '*']],
            \ {'g':{'h':{'i': {'none': '*'}, 'options': {'capital': 'none'}}}}],
            \['setoption', ['capital', 'first', 'gh', 1],
            \ [['GHI', '*'],
            \  ['ghi', '*']],
            \ {'g':{'h':{'i': {'none': '*'}, 'options': {'capital': 'first'}}}}],
            \['deloption', ['capital', 'gh'],
            \ [['GHI', '*'],
            \  ['ghi', '*']],
            \ {'g':{'h':{'i': {'none': '*'}}}}],
            \['setoption', ['capital', 'none', 'ghi', 0],
            \ [['GHI', 'GHI'],
            \  ['ghi', '*']],
            \ {'g':{'h':{'i': {'none': '*', 'options': {'capital': 'none'}}}}}],
            \['del', ['g', 1],
            \ [['ab',  'ab' ],
            \  ['abc', 'abc'],
            \  ['ghi', 'ghi']],
            \ {}],
            \['include', ['|', 'test-alpha'],
            \ [['|a', 'α'],
            \  ['a',  'a']],
            \ {'|':{'include': ['test-alpha']}}],
            \['include', ['|', 'test-alpha'],
            \ [['|a', 'α'],
            \  ['a',  'a']],
            \ {'|':{'include': ['test-alpha']}}],
            \['exclude', ['|', 'test-alpha'],
            \ [['|a', '|a'],
            \  ['a',  'a' ]],
            \ {}],
            \['add', ['||', '--'],
            \ [['||', '--']],
            \ {'|':{'|': '--'}}],
            \['include', ['|', 'test-alpha'],
            \ [['|a', 'α' ],
            \  ['a',  'a' ],
            \  ['||', '--']],
            \ {'|':{'include': ['test-alpha'],'|': '--'}}],
            \['del', ['|', 1],
            \ [['|a', '|a'],
            \  ['a',  'a' ],
            \  ['||', '||']],
            \ {}],
            \['add', ['|', '-'],
            \ [['||', '--']],
            \ {'|': '-'}],
            \['include', ['|', 'test-alpha'],
            \ [['|a', 'α' ],
            \  ['a',  'a' ],
            \  ['||', '--']],
            \ {'|':{'include': ['test-alpha'],'none': '-'}}],
            \['del', ['|', 1],
            \ [['|a', '|a'],
            \  ['a',  'a' ],
            \  ['||', '||']],
            \ {}],
        \]
let getres={
            \'file': 'eval(join(readfile(Arg, "b"), ""))',
            \'var':  'eval(Arg)',
            \'dict': 'Arg',
            \'func': 'call(Arg, [], {})',
            \'conf': 'eval(join(readfile(Arg.".json", "b"), ""))',
        \}
let errfile=g:curtest.'.fail'
for [src, Arg] in addremovesources+addremovesources
    for test in addremovetests
        try
            silent call call('Tr3'.test[0], test[1]+[Arg])
        endtry
        for [in, out] in test[2]
            let result=Tr3transliterate(in, Arg)
            if out!=#result
                call writefile(['   Arg: '.string(Arg),
                            \   '    in: '.string(in),
                            \   '   out: '.string(out),
                            \   'result: '.string(result)], errfile, 'b')
                break
            endif
        endfor
        let tresult=eval(getres[src])
        if tresult!=#test[3]
            call writefile(['    Arg: '.string(Arg),
                        \   'tresult: '.string(tresult),
                        \   'test[3]: '.string(test[3])], errfile, 'b')
            break
        endif
    endfor
    unlet Arg
endfor
