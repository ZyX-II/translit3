function! g:WriteFunc(str)
    execute 'normal! I'.a:str
endfunction
let toftests=[
            \'Tr3Command tof start',
            \[''],
            \['abc'],
            \['abc%def%ghi'],
            \[['aabc', 'a%%d', 'ae', 'af ghi']],
            \[['ac', "ah\ea"], ['ч']],
            \[['a ab', 'T ic', 'ah$', 'ai', 'ai', 'ai']],
            \['$iii'],
            \[["a-", 'I$', 'ai', 'ai', 'ai']],
            \'setlocal tw=10 fo+=an12wcroqlt autoindent',
            \['abc def $iiihi jkl mno pqr'],
            \['1. abc def $iiihi jkl mno pqr'],
            \'setlocal tw=80',
            \{'NoTransWord': {}},
            \[['aabc', 'a%%def ghi']],
            \{'NoTransWord': {'%%': ''},
            \ 'StopTrSymbs': {}},
            \[['aabc', 'a%%d', 'ae', 'af ghi']],
            \['abc%def% ghi'],
            \{'StopTrSymbs': {'%': '|'},
            \ 'StartTrSymbs': {'%': '@'}},
            \['abc%def% ghi'],
            \{'StopTrSymbs': {'%': ''},
            \ 'StartTrSymbs': {'%': ''},
            \ 'BrkSeq': ''},
            \['@@@'],
            \{'BrkSeq': '@'},
            \['@@@'],
            \['$iii'],
            \['$i@ii'],
            \'set delcombine',
            \['a`a``'],
            \'set nodelcombine',
            \['a`a``'],
            \'set backspace=',
            \['a`a``'],
            \'set delcombine',
            \['a`a``'],
            \'set nodelcombine',
            \['a`a``'],
            \'set backspace&vim',
            \{'WriteFunc': 'g:WriteFunc'},
            \['abc'],
            \{'WriteFunc': 0},
        \]
for test in toftests
    if type(test)==type([])
        let [in; dummy]=test
        normal! o
        if type(in)==type('')
            for char in split(in, '\zs')
                execute 'normal A'.char
            endfor
        else
            for char in in
                execute 'normal '.char
            endfor
        endif
        unlet in
    elseif type(test)==type('')
        execute test
    elseif type(test)==type({})
        call extend(g:tr3Options, test)
        Tr3Command tof restart
    endif
    unlet test
endfor
