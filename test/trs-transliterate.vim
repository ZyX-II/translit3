function CustomPlugin(match, tail, transsymb, cache, flags)
    return      {'status': 'success',
                \'delta': -2,
                \'result': a:match,
                \'flags': a:flags,}
endfunction
function CustomPlugin2(match, tail, transsymb, cache, flags) dict
    return      {'status': 'pass',
                \'delta': -len(a:match),
                \'result': a:match,
                \'flags': a:flags,}
endfunction
function CustomPlugin3(match, tail, transsymb, cache, flags) dict
    return      {'status': 'success',
                \'delta': 0,
                \'result': '!!'.a:match,
                \'flags': a:flags,}
endfunction
let defaulttests=[
            \'',
            \'abc',
            \'a\bc',
            \'\\',
            \'\',
            \'d%\bc%',
            \'d%\bc',
            \'d%bc',
            \'d%b%c',
            \'d%b\%c',
            \'d\%b%c',
            \'$%abc',
            \'abc def',
            \'abc %%def ghi',
            \'abc %%def',
            \'%%abc def',
            \'%% def',
            \'%%\ def',
            \'$\|a',
            \'@@@',
            \'$iii',
            \'$i@ii',
            \'$ii@i',
            \'$Iii',
            \'ABC',
            \'$iIi',
            \'u''',
            \'ux',
            \'Ux',
            \'U''',
            \'UX',
            \'uX',
            \'a@',
            \'%%i@',
            \'$ii\@i',
            \'%@i%',
            \{'BrkSeq': 'bbb'},
            \'bb',
            \'bbb',
            \'$ibbbii',
            \{'BrkSeq': '@',
            \ 'EscSeq': ''},
            \'\bc',
            \'%\bc',
            \{'BrkSeq': '',
            \ 'EscSeq': '\'},
            \'ab@c',
            \{'BrkSeq': '@',
            \ 'EscSeq': '@'},
            \'@@@',
            \'%@%',
            \'%@@@',
            \{'EscSeq': '!!'},
            \'!!%',
            \'%!!%',
            \{'EscSeq': '\',
            \ 'StopTrSymbs': {'%': '@'}},
            \'d%abc%',
            \'d%abc',
            \{'StartTrSymbs': {'%': '@'}},
            \'d%abc%',
            \{'StartTrSymbs': {'%': '',
            \                  '*': '*'},
            \ 'StopTrSymbs': {'%': '',
            \                 '*': '-'}},
            \'d*abc%',
            \'d%abc*',
            \'d*abc*',
            \{'StartTrSymbs': {'%': ''},
            \ 'StopTrSymbs': {'%': ''},
            \ 'NoTransWord': {'*': '@',
            \                 '%': ''}},
            \'%abc def',
            \'*abc def',
            \{'NoTransWord': {'%%': ''},
            \ 'Plugs': {'Before': ['brk', 'esc'],
            \           'After': ['notransword', 'notrans']}},
            \'$\|a',
            \{'Plugs': {'Before': ['brk', 'esc', 'notrans']}},
            \'$%abc',
            \'$%abc%def',
            \'$%abc\%def',
            \{'Plugs': {'Before': ['brk', 'esc', 'notrans', 'notransword']}},
            \'$%%abc def',
            \{'Plugs': {'Before': ['brk', 'esc', 'notransword', 'notrans']}},
            \'$%%abc def',
            \{'Plugs': {}},
            \'abc %def% %%ghi \jkl',
            \{'Plugs': {'Before': [[function('CustomPlugin'), '...']]}},
            \'abc',
            \{'Plugs': {'Before': [[function('CustomPlugin2'), '...']]}},
            \'abc',
            \{'Plugs': {'After': [[function('CustomPlugin3'), '.']]}},
            \'abc%def',
            \{'Plugs': {'Before': ['brk'],
            \           'After': ['esc', 'notransword',
            \                     'notrans']},
            \ 'DefaultTranssymb': directory.'/transsymb-ru:latex.json'},
            \'|g',
            \'|G',
            \{'DefaultTranssymb': directory.'/transsymb.json'},
        \]
let results=[]
for test in defaulttests
    if type(test)==type('')
        let result=Tr3transliterate(test)
        call add(results, result)
    elseif type(test)==type({})
        call extend(g:tr3Options, test)
    endif
    unlet test
endfor
call writefile(results, g:outfile, 'b')
